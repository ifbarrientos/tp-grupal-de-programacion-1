package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {

	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	private Image imagen;

	public Princesa(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 20;
		this.alto = 50;
		this.velocidad = 5;
		this.imagen = Herramientas.cargarImagen("princesa.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(imagen, x, y, 0, 0.50);
	}

	public boolean chocasteconParedDerecha(Entorno entorno) {
		return x > entorno.ancho() / 2 - ancho / 2;
	}

	public boolean chocasteconParedIzquierda(Entorno entorno) {
		return x < entorno.ancho() / 16;
	}

	public void moverIzquierda(Entorno entorno) {
		if (!chocasteconParedIzquierda(entorno)) {
			x -= velocidad;
		}
	}

	public void moverDerecha(Entorno entorno) {
		if (!chocasteconParedDerecha(entorno))
			x += velocidad;
	}

	public void salto(Entorno entorno) {
		y -= 170;
	}

	public boolean estaEnElpiso(Entorno entorno) {
		return y == entorno.alto() / 1.27;
	}

	public void caer() {
		y += 2;
	}

	public void disparar(BolaDeFuego[] bolaDeFuego) {
		for (int i = 0; i < bolaDeFuego.length; i++) {
			if (bolaDeFuego[i] == null) {
				bolaDeFuego[i] = new BolaDeFuego(x, y, 20, 20);
				break;
			}
		}
	}

	public int getAncho() {
		return ancho;
	}
	public double getX() {
		return x;
	}
	public int getAlto() {
		return alto;
	}
	public double getY() {
		return y;
	}
}