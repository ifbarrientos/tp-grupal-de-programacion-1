package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Choque {
	private double x;
	private double y;
	private Image img;

	public Choque(double x, double y) {
		this.x = x;
		this.y = y;
		this.img = Herramientas.cargarImagen("choque.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(img, x, y, 0, 1.2);
	}
}
