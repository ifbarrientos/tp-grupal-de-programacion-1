package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Enemigo {

	private double x;
	private double y;
	private double velocidad;
	private int ancho;
	private int alto;
	private Image img;

	public Enemigo(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 30;
		this.alto = 30;
		this.velocidad = 4;
		this.img = Herramientas.cargarImagen("bicho.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(img, x, y, 0, 0.45);
	}

	public void moverIzquierda() {
		x -= velocidad;
	}

	public static void borrarEnemigo(Enemigo[] enemigos, Entorno entorno) {
		for (int i = 0; i < enemigos.length; i++) {
			if (enemigos[i] != null && enemigos[i].x < (entorno.ancho() / 6) - 180) {
				enemigos[i] = null;
			}
		}
	}

	public boolean chocasteConPrincesa(Princesa pr) {
		if (x <= pr.getX() && y == pr.getY() && !sePaso(pr)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean sePaso(Princesa pr) {
		if (x <= pr.getX() - 15) {
			return true;
		} else {
			return false;
		}
	}
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public int getAncho() {
		return ancho;
	}
	public int getAlto() {
		return alto;
	}
	public double getVelocidad() {
		return velocidad;
	}
}