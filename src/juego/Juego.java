package juego;

import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;
import java.util.concurrent.ThreadLocalRandom;

public class Juego extends InterfaceJuego {

	private int vidas;
	private int puntos;
	private int cantidadDeTicks;
	private boolean gameOver;
	private boolean estamosEnColision;
	private Entorno entorno;
	private BolaDeFuego[] bolasDeFuego; // se puso en plural
	private Obstaculo[] obstaculos;
	private Princesa princesa;
	private Enemigo[] enemigos;
	private Choque choque;
	private Image fondo;
	private Image negro;

	public Juego() {
		this.gameOver = false;
		this.vidas = 3;
		this.entorno = new Entorno(this, "Super Elizabeth Sis", 800, 600);

		princesa = new Princesa(entorno.ancho() / 4, entorno.alto() / 1.27);

		fondo = Herramientas.cargarImagen("fondo.jpg");
		negro = Herramientas.cargarImagen("negro.jpg");

		obstaculos = new Obstaculo[2];
		bolasDeFuego = new BolaDeFuego[3];
		enemigos = new Enemigo[3];

		enemigos[0] = new Enemigo((entorno.ancho() / 0.45), entorno.alto() / 1.27);
		enemigos[1] = new Enemigo((entorno.ancho() / 0.4) + 1, entorno.alto() / 1.27);
		enemigos[2] = new Enemigo((entorno.ancho() / 0.5) + 3, entorno.alto() / 1.27);

		obstaculos[0] = new Obstaculo(entorno.ancho() / 0.25, entorno.alto() / 1.27);
		obstaculos[1] = new Obstaculo(entorno.ancho() / 0.40, entorno.alto() / 1.27);

		estamosEnColision = false;
		this.entorno.iniciar();
	}

	public void tick() {
		cantidadDeTicks++;
		if (gameOver) {
			entorno.dibujarImagen(negro, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);
			entorno.dibujarCirculo(entorno.ancho() / 3, entorno.alto() / 3, 0, Color.WHITE);
			entorno.escribirTexto("Perdiste", entorno.ancho() / 2, entorno.alto() / 2);
			return;
		}

		entorno.dibujarImagen(fondo, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);
		princesa.dibujar(entorno);

		if (cantidadDeTicks < 40 && estamosEnColision) {
			choque = new Choque(princesa.getX(), princesa.getY());
			choque.dibujar(entorno);
		}

		if (cantidadDeTicks == 30) {
			estamosEnColision = false;
		}

		for (int i = 0; i < vidas; i++) {
			entorno.dibujarCirculo(entorno.ancho() / 10 + i * 20 - 5, entorno.alto() / 10, 20, Color.RED);
			entorno.escribirTexto("Vidas", entorno.ancho() / 10, entorno.alto() / 20);
			entorno.escribirTexto("Puntos", entorno.ancho() - 150, entorno.alto() / 20);
			entorno.escribirTexto(String.valueOf(puntos), entorno.ancho() - 5 * 20 - 5, entorno.alto() / 20);
		}

		for (Enemigo s : enemigos) {
			if (s != null) {
				s.dibujar(entorno);
				s.moverIzquierda();
			}
		}

		for (BolaDeFuego p : bolasDeFuego) {
			if (p != null) {
				p.dibujar(entorno);
				p.moverDerecha();
			}
		}

		for (Obstaculo o : obstaculos) {
			if (o != null) {
				o.dibujar(entorno);
				o.moverIzquierda();
			}
		}

//		 los soldados deberian aparecer aleatoreamente, por ahi te conviene poner un random en el constructor del soldado un mas/menos algo en x, para que no aparezcan siempre en la misma posicion
		
		int randomNum1 = ThreadLocalRandom.current().nextInt(1, 3 + 1); // se usa esto para generar un numero aleatorio	

		for (int i = 0; i < enemigos.length; i++) {
			if (enemigos[i] == null) {
				enemigos[i] = new Enemigo((entorno.ancho() / 0.45) + randomNum1, entorno.alto() / 1.27);	
				break;
			}
		}

		for (int j = 0; j < obstaculos.length; j++) {
			if (obstaculos[j] == null) {
				obstaculos[j] = new Obstaculo(entorno.ancho() / 0.20, entorno.alto() / 1.27);
				break;
			}
		}

		Enemigo.borrarEnemigo(enemigos, entorno);
		BolaDeFuego.borrarBolaDeFuego(bolasDeFuego, entorno);
		Obstaculo.borrarObstaculo(obstaculos, entorno);

		if (!princesa.estaEnElpiso(entorno)) {
			princesa.caer();
		}

		if (entorno.estaPresionada(entorno.TECLA_ARRIBA) && princesa.estaEnElpiso(entorno)
				|| (entorno.estaPresionada('w')) && princesa.estaEnElpiso(entorno)) {
			princesa.salto(entorno);
		}

		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && princesa.estaEnElpiso(entorno)
				|| entorno.estaPresionada('a') && princesa.estaEnElpiso(entorno)) {
			princesa.moverIzquierda(entorno);
		}

		if (entorno.estaPresionada(entorno.TECLA_DERECHA) && princesa.estaEnElpiso(entorno)
				|| entorno.estaPresionada('d') && princesa.estaEnElpiso(entorno)) {
			princesa.moverDerecha(entorno);
		}

		if (entorno.sePresiono(entorno.TECLA_ESPACIO)) {
			princesa.disparar(bolasDeFuego);
		}

		for (int i = 0; i < enemigos.length; i++) {
			if (enemigos[i] != null && enemigos[i].chocasteConPrincesa(princesa)) {
				enemigos[i] = null;
				vidas--;
				estamosEnColision = true;
				cantidadDeTicks = 0;
			}
		}

		for (int i = 0; i < obstaculos.length; i++) {
			if (obstaculos[i] != null && obstaculos[i].chocasteConPrincesa(princesa)) {
				obstaculos[i] = null;
				vidas--;
				estamosEnColision = true;
				cantidadDeTicks = 0;
			}
		}

		for (int i = 0; i < bolasDeFuego.length; i++) {
			if (bolasDeFuego[i] != null && bolasDeFuego[i].chocasteConEnemigo(enemigos[i])) {
				bolasDeFuego[i] = null;
				enemigos[i] = null;
				puntos = puntos + 5;
			}
		}

		if (vidas <= 0) {
			this.gameOver = true;
		}

	}

	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}