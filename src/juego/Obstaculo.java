package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {

	private double x;
	private double y;
	private double velocidad;
	private int alto;
	private int ancho;
	private Image img;

	public Obstaculo(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 20;
		this.alto = 60;
		this.velocidad = 3;
		this.img = Herramientas.cargarImagen("tuberia.png");
	}
	
	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(img, x, y, 0, 0.50);
	}
	
	public void moverIzquierda() {
		x -= velocidad;
	}
	
	public boolean chocasteConPrincesa(Princesa pr) {
		if (x <= pr.getX() && y == pr.getY() && !sePaso(pr)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean sePaso(Princesa pr) {
		if (x <= pr.getX() - 15) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void borrarObstaculo(Obstaculo[] ob, Entorno entorno) {
		for (int i = 0; i < ob.length; i++) {
			if (ob[i] != null && ob[i].x < entorno.ancho() / 6 - 180) {
				ob[i] = null;
			}
		}
	}
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public int getAlto() {
		return alto;
	}
	public int getAncho() {
		return ancho;
	}
}