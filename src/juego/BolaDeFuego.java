package juego;

import java.awt.Color;
import entorno.Entorno;

public class BolaDeFuego {
	private double x;
	private double y;
	private double diametro;
	private double velocidad;

	public BolaDeFuego(double x, double y, double diametro, double velocidad) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		this.velocidad = 20;

	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarCirculo(x, y, diametro, Color.ORANGE);
	}

	public static void borrarBolaDeFuego(BolaDeFuego[] pelotas, Entorno entorno) {
		for (int i = 0; i < pelotas.length; i++) {
			if (pelotas[i] != null && pelotas[i].x > entorno.ancho()) {
				pelotas[i] = null;
			}
		}
	}

	public void moverDerecha() {
		x += velocidad;
	}

	public boolean chocasteConEnemigo(Enemigo s) {
		return s.getX() - s.getAncho() / 2 <= x && x <= s.getX() + s.getAncho() / 2
				&& y + diametro / 2 > s.getY() - s.getAlto() / 2;
	}

	public double getDiametro() {
		return diametro;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
}